<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SentShip</title>
    <link rel="icon" href="img/b-logo.png">
    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <!-- Font Awesome -->
    <link href="node_modules/@fortawesome/fontawesome-free/css/all.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Merriweather:400,900,900i" rel="stylesheet">
    <!-- Main CSS -->
    <link rel="stylesheet" href="css/main.css">
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>
    <!-- jQuery -->
    <script src="node_modules/jquery/dist/jquery.min.js"></script>
</head>
<body class="hidden">
    <script>
        jQuery('body').css('display', 'none');
        jQuery(document).ready(function () {
            jQuery('body').fadeIn(1000);
        });
    </script>
    <!-- First section -->
    <div class="first-div">
        <!-- Navbar -->
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand text-white" href="index.html"><img src="img/logo.png"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
        
            <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link text-white home" href="#">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-white" href="#">Pricing</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-white" href="#">Sales</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-white track" href="#">
                            Tracking shipment
                            <i class="fas fa-chevron-down"></i>
                            <i class="fas fa-chevron-up"></i>
                        </a>
                    </li>
                </ul>
                <div class="dropdown">
                    <button type="button" class="btn btn-outline-light my-2 my-sm-0 rounded-pill login" data-toggle="dropdown" data-display="static"
                        id="dropdownMenuOffset" aria-haspopup="true" aria-expanded="false">
                        LOGIN
                    </button>
                    <div class="dropdown-menu dropdown-menu-lg-right my-4 p-4" aria-labelledby="dropdownMenuOffset">
                        <div class="form-group">
                            <input type="email" class="form-control my-5" id="exampleDropdownFormEmail2" placeholder="Username">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control mb-5" id="exampleDropdownFormPassword2" placeholder="Password">
                        </div>
                        <form action="" class="d-flex justify-content-center">
                            <button type="submit" class="btn btn-outline-primary my-2 my-sm-0 rounded-pill login2">LOGIN</button>
                        </form>
                    </div>
                </div>
            </div>
        </nav>
        <!-- Section -->
        <div class="section">
            <h4 class="font-weight-bold">Deliver what you want ,safely!</h4>
            <h1 class="font-weight-bold">Your Ultimate<br>Courier Partner ;)</h1>
            <h5>Deliver your products to any place in KSA with a one-stop,<br>full service next day delivery and logistics solution.</h5>
            <div class="section-btn">
                <a class="btn btn-outline-light my-2 my-sm-0 rounded-pill text-white button font-weight-bold" href="#"><h6>Get start</h6></a>
                <a class="btn btn-outline-light my-2 my-sm-0 rounded-pill text-white button font-weight-bold" href="#"><h6>Contact sales</h6></a>
            </div>
        </div>
        <!-- Section 2-->
        <div class="track-div section2">
            <form action="">
                <div class="input-group mb-3">
                    <input type="text" class="d-flex flex-row-reverse bd-highlight form-control"
                        placeholder="Please enter shipment order no like 4955220351" aria-label="order no"
                        aria-describedby="basic-addon1">
                    <div class="input-group-append">
                        <button class="btn" type="button" id="button-addon2">GO</button>
                    </div>
                </div>
            </form>
            <div class="card tracking">
                <div class="card-body container">
                    <div class="row details">
                        <div class="col-md font-weight-bold">
                            <span class="dot"></span> Order placed on: 22 March 2020
                        </div>
                        <div class="col-md font-weight-bold">
                            <span class="dot"></span> Recipient: Mohamed Shallan
                        </div>
                    </div>
                    <div class="row details">
                        <div class="col-md font-weight-bold">
                            <span class="dot"></span> Order ID: #4058276638032
                        </div>
                        <div class="col-md font-weight-bold">
                            <span class="dot"></span> Payment method: Cash on delivery (COD)
                        </div>
                    </div>
                    <div class="row details">
                        <div class="col-md font-weight-bold">
                            <span class="dot"></span> Condition: <span class="font-weight-bold"
                                style="color: #4DFE43;">ACTIVE</span>
                        </div>
                        <div class="col-md font-weight-bold">
                            <span class="dot"></span> Total: 276.00 SR
                        </div>
                    </div>
                    <div class="row details">
                        <div class="col-md font-weight-bold">
                            <span class="dot"></span> Store: Egypt Laptops
                        </div>
                        <div class="col-md font-weight-bold">
                            <span class="dot"></span> Address: G155 faisel street, Elking square, Jeddah
                        </div>
                    </div>
                    <div class="wrapper">
                        <div class="timeline">
                            <div class="line-area"></div>
                            <div class="content font-weight-bold">
                                <span>Received</span>
                            </div>
                            <div class="content font-weight-bold">
                                <span>Delivering</span>
                            </div>
                            <div class="content font-weight-bold">
                                <span>Delivered</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Second section -->
    <div class="second-div bg-light">
        <h6 class="d-flex justify-content-center text-danger font-weight-bold">ABOUT</h6>
        <h3 class="d-flex justify-content-center font-weight-bold">The Ultimate Solution</h3>
        <div class="row justify-content-between cards">
            <div class="card col-md-3 card1">
                <div class="card-body">
                    <i class="fas fa-box-open fa-5x card-1"></i>
                    <h5 class="card-title">Track orders</h5>
                    <p class="card-text text-muted">Using your home computer you can unlock the bible codes and explore history on your own.</p>
                </div>
            </div>
            <div class="card col-md-3 card2">
                <div class="card-body">
                    <i class="far fa-credit-card fa-5x card-2"></i>
                    <h5 class="card-title">Cash collection</h5>
                    <p class="card-text text-muted">Real-time insights on your cash collections through sentship cash collection service.</p>
                </div>
            </div>
            <div class="card col-md-3 card3">
                <div class="card-body">
                    <i class="fas fa-exchange-alt fa-5x card-3"></i>
                    <h5 class="card-title">Customer returns</h5>
                    <p class="card-text text-muted">From second thoughts to total satisfication, sentship handles customer returns.</p>
                </div>
            </div>
        </div>
        <div class="d-flex justify-content-center">
            <a class="btn btn-outline-light my-2 my-sm-0 rounded-pill text-white button button2" href="#"><h6>Get start</h6></a>
        </div>
    </div>
    <!-- third section -->
    <div class="third-div">
        <div class="row justify-content-between">
            <div class="col-md-4 same">
                <h4><img src="img/b-logo.png"></h4>
                <p class="text-muted">+095xxx995663<br>hello@sentship.com</p>
                <a href="#"><i class="fab fa-facebook-square fa-3x"></i></a>
                <a href="#"><i class="fab fa-tumblr-square fa-3x"></i></a>
            </div>
            <div class="col-md-3">
                <h2 class="font-weight-bold">Shipments</h2>
                <ul>
                    <li>
                        <a class="text-muted" href="#">Our pricing</a>
                    </li>
                    <li>
                        <a class="text-muted" href="#">Tracking orders</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3">
                <h2 class="font-weight-bold">About</h2>
                <ul>
                    <li>
                        <a class="text-muted" href="#">Contact sales</a>
                    </li>
                    <li>
                        <a class="text-muted" href="#">Terms & conditions</a>
                    </li>
                    <li>
                        <a class="text-muted" href="#">Privacy policy</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- Footer -->
    <div class="forth-div">
        <p class="d-flex justify-content-center text-white">Copyrights &copy;2020 sentship.com</p>
    </div>
    <a id="button"></a>
    <!-- Scripts -->
    <script>
        $('document').ready(function () {
            $(".fa-chevron-down").show();
            $(".fa-chevron-up").hide();
            $(".section").show();
            $(".section2").hide();
            $(".home").on('click', function (e) {
                jQuery('.section').fadeIn(100);
                $(".fa-chevron-down").show();
                $(".fa-chevron-up").hide();
                $(".section").show();
                $(".section2").hide();
                $('.navbar-collapse').removeClass('show');
            });
            $(".track").on('click', function (e) {
                jQuery('.section2').fadeIn(100);
                $(".fa-chevron-down").hide();
                $(".fa-chevron-up").show();
                $(".section").hide();
                $(".section2").show();
                $(".navbar-collapse").removeClass('show');
            });
        });
    </script>
    <script>
        jQuery('.card1').css('display', 'none');
        jQuery('.card2').css('display', 'none');
        jQuery('.card3').css('display', 'none');
        jQuery('.button2').css('display', 'none');
        $(window).scroll(function () {
            if ($(window).scrollTop() > 300) {
                $(".card1").fadeIn(1000);
                $(".card2").fadeIn(2000);
                $(".card3").fadeIn(3000);
                $(".button2").fadeIn(3000);
            }
        });
    </script>
    <script>
        var btn = $('#button');
        $(window).scroll(function () {
            if ($(window).scrollTop() > 300) {
                btn.addClass('show');
            } else {
                btn.removeClass('show');
            }
        });
        btn.on('click', function (e) {
            e.preventDefault();
            $('html, body').animate({ scrollTop: 0 }, '300');
        });
    </script>
    <script>
        $('.navbar-nav .nav-item').on('click', function () {
            $('.nav-item').removeClass('active');
            $(this).addClass('active');
        });
    </script>
</body>
</html>