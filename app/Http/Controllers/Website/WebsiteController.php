<?php

namespace App\Http\Controllers\Website;

use Illuminate\Http\Request;
use App\Mail\SendQuestionMail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\Website\UploadFunQuestionRequest;
use App\Http\Requests\Website\UploadKnowledgeQuestionRequest;

class WebsiteController extends Controller
{
    public function get_index()
    {
        return view('website.index');
    }

    public function get_tracking()
    {
        return view('website.tracking');
    }

    public function get_policy()
    {
        return view('website.policy');
    }

    public function get_terms()
    {
        return view('website.terms');
    }
    public function get_policyAr()
    {
        return view('website.policyAr');
    } 
    public function get_termsAr()
    {
        return view('website.termsAr');
    }

}
