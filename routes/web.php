<?php

use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'Website'], function () {

    Route::get('/', 'WebsiteController@get_index')->name('index');
    Route::get('/tracking', 'WebsiteController@get_tracking')->name('tracking');
});